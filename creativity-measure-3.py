import csv
import math
with open('test-songs.csv', 'rb') as f:
    reader = csv.reader(f)
    songs = list(reader)
	
f.close()

songs = filter(None, songs)

columns_labels = songs[0]
for j in range (0,len(columns_labels)):
	if columns_labels[j].strip() == 'tags':
		tags_index = j
	if columns_labels[j].strip() == 'playcount':
		playcount_index = j

songs = songs[1:len(songs)]
		
total_playcounts = 0
for song in songs:
	total_playcounts = total_playcounts + int(song[playcount_index])

song_values = []
song_proportions = []
list_all_tags = set([])
for song in songs:
	# total playcount
	song_values.append((int(song[playcount_index]) * 1.0) / total_playcounts)
	
	# change counts to proportions
	total_count = 0
	tags = song[tags_index].split('],[')
	tags[0] = tags[0][2:len(tags[0])]
	tags[len(tags) - 1] = tags[len(tags) - 1][0:len(tags[len(tags) - 1])-2]
	
	single_song_proportion = {}
	for tag in tags:
		count = tag.split(',')
		total_count = total_count + int(count[1].strip())
		
	for tag in tags:
		count = tag.split(',')
		count[0] = count[0].strip()
		count[0] = count[0].replace("\"","")
		list_all_tags.add(count[0])
		single_song_proportion[count[0]] = (int(count[1].strip()) * 1.0) / total_count
			
	song_proportions.append(single_song_proportion)	

# calculate mainstream
mainstream_song_proportions = {}	
for tag in list_all_tags:
	mainstream_song_proportions[tag] = 0

for tag in list_all_tags:
	for song in song_proportions:
		try:
			pr = song[tag] 
			mainstream_song_proportions[tag] = mainstream_song_proportions[tag] + pr
		except KeyError:
			pass

# enumerate all tags			
for tag in list_all_tags:
	mainstream_song_proportions[tag] = mainstream_song_proportions[tag]/len(song_proportions)

# calculate originality of a single tag per song
single_tag_originality_per_song = []
for song in song_proportions:
	originality_per_tag = 0
	for tag in song:
		originality_per_tag = originality_per_tag + math.sqrt((song[tag] - mainstream_song_proportions[tag])* (song[tag] - mainstream_song_proportions[tag]))
	
	originality_per_tag = originality_per_tag/len(song)
	single_tag_originality_per_song.append(originality_per_tag)
		
# enumerate all pairs of tags
tag_pairs = set([])
for song in song_proportions: 	
	tags = song.keys()
	for i in range(0, len(tags)):
		for j in range(i+1, len(tags)):
			if ((tags[i], tags[j]) not in tag_pairs and (tags[j], tags[i]) not in tag_pairs):
				tag_pairs.add((tags[i], tags[j]))

# calculate mainstream ration of each pair of tags
mainstream_ratio_per_pair_tags = {}
for tag_pair in tag_pairs:
	mainstream_ratio_per_pair_tags[tag_pair] = 0
	for song in song_proportions:
		try:
			tag1 = song[tag_pair[0]]
			tag2 = song[tag_pair[1]]
			# total number of tag_pair in all the songs
			mainstream_ratio_per_pair_tags[tag_pair] = mainstream_ratio_per_pair_tags[tag_pair] + 1
		except KeyError:
			pass
	# Original ratio of every pair of tags in all the songs
	mainstream_ratio_per_pair_tags[tag_pair] = 1- ((mainstream_ratio_per_pair_tags[tag_pair] * 1.0)/len(song_proportions))

# calculate originality of a pair of tags per song
pair_tags_originality_per_song = []
for song in song_proportions:
	originality_per_tag_pair = 0
	total_proportion_pair_one_song = 0
	tags = song.keys()
	for i in range(0, len(tags)):
		for j in range(i+1, len(tags)):
			try:
				total_proportion_pair_one_song = total_proportion_pair_one_song + (song[tags[i]] * song[tags[j]])
				originality_per_tag_pair = originality_per_tag_pair + (mainstream_ratio_per_pair_tags[(tags[i], tags[j])] * song[tags[i]] * song[tags[j]])
			except KeyError:
				total_proportion_pair_one_song = total_proportion_pair_one_song + (song[tags[i]] * song[tags[j]])
				originality_per_tag_pair = originality_per_tag_pair + (mainstream_ratio_per_pair_tags[(tags[j], tags[i])] * song[tags[i]] * song[tags[j]])
	
	originality_per_tag_pair = (originality_per_tag_pair*1.0)/total_proportion_pair_one_song
	pair_tags_originality_per_song.append(originality_per_tag_pair)
	
# calculate total oiginality and creativity per song
originality_per_song = []
creativity_per_song = []
for i in range(0, len(song_proportions)):
	originality = (single_tag_originality_per_song[i] + pair_tags_originality_per_song[i])/2
	originality_per_song.append(originality)
	creativity_per_song.append((originality + song_values[i])/2)


print 'creativity score per song:'
print creativity_per_song
print 'originality score per song:'
print originality_per_song
print 'originality for pair of tags per song:'
print pair_tags_originality_per_song
print 'originality for single tag per song:'
print single_tag_originality_per_song
#print mainstream_ratio_per_pair_tags
#print mainstream_song_proportions			
#print song_proportions	
print 'values per song:'
print song_values

		
		

	
	


